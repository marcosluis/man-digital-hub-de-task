CREATE TABLE mdh_ranking_by_county
WITH (
  format='PARQUET',
  external_location='s3://man-digital-hub-de-task/mhd_ranking_by_county/'
) AS

SELECT 
policyid,
county,
max(eq_site_limit) as highest_eq_site_limit, 
rank() over (PARTITION BY county ORDER BY max(eq_site_limit) DESC) AS rnk
FROM "man-digital-hub"."mdh_source_data"
group by policyid, county
order by 4;