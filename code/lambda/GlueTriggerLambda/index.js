var AWS = require('aws-sdk');
var glue = new AWS.Glue();
var sqs = new AWS.SQS();
	exports.handler = function(event, context,callback) {
		console.log(JSON.stringify(event, null, 3));
		if(event.Records.length > 0 && event.Records[0].eventSource == 'aws:sqs'){
			startCrawler('MANGlueCrawler', function(err2,data2){
				if(err2) callback(err2)
				else callback(null,data2)
			})
		}else{
		var dbName = 'ManDigitalDB';
		var params = {
			DatabaseInput: {
				Name: dbName,
				Description: 'Metro LA database',
			}
		};
		glue.createDatabase(params, function(err, data) {
				var params1 = {
					DatabaseName: dbName,
					Name: 'MANGlueCrawler',
					Role: 'service-role/mandigitaltask-GlueLabRole-JNZ6F30Z4MBP',
					Targets: {
						S3Targets: [{ Path: 's3://mandigitaltask-raws3bucket-tuoxlklh1kxo' }]
					},
					Description: 'crawler test'
				};
				glue.createCrawler(params1, function(err1, data1) {
					startCrawler('MANGlueCrawler', function(err2,data2){
						if(err2) callback(err2)
						else callback(null,data2)
					})
				});
		});
	};
};
function startCrawler(name,callback){
	var params = {
		Name: name,
	};
	glue.startCrawler(params, function(err, data) {
		if (err){
			console.log(JSON.stringify(err,null,3 ))
			var params1 = {
				MessageBody: 'retry',
				QueueUrl: 'https://sqs.eu-west-1.amazonaws.com/459602882924/mandigitaltask-SQSqueue-15YIUQ04R95H'
			};
			sqs.sendMessage(params1, function(err1, data1) {
				if (err1) callback(err1);
				else     callback(null, data1)
			});
		}
		else{
			callback(null, data)
		}
	});
	}