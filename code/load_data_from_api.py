import requests 
import pandas as pd 
import json
import datetime 
import boto3

# AWS config 
aws_access_key = ''
aws_secret_key = ''
aws_region = 'eu-west-1'
session = boto3.Session(
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
    region_name=aws_region)

s3 = boto3.client('s3')

# load vehicle data from LA Metro API
api_url = 'https://api.metro.net/agencies/lametro/vehicles/'

l = []
r = requests.get(url=api_url)
j = r.json()
j2 = j['items']

print('Getting data from Metro LA API...\n')
for s in j2:
    if 'run_id' in s:
        item_dict_s = {
            'route_id': s['route_id'],
            'id': s['id'],
            'run_id': s['run_id'],
            'predictable': s['predictable'],
            'seconds_since_report': s['seconds_since_report'],
            'heading': s['heading'],
            'latitude': s['latitude'],
            'longitude': s['longitude'],
            'current_date': datetime.datetime.now(),
            'original_source': 'metro_la_api'
        }
    l.append(item_dict_s)
print('Generating the file to be uploaded to Amazon S3 ...\n')
output_file_name = 'vehicle_data_{0}.csv'.format(datetime.datetime.now().timestamp())
df = pd.DataFrame(l)
df.to_csv(output_file_name)

print('Uploading the file to Amazon S3 ...\n')
# Upload to S3
s3.upload_file(
    Bucket='bucket',
    Key='metro_la_data/{0}'.format(output_file_name), 
    Filename='{0}'.format(output_file_name))

print('DONE ...\n')
