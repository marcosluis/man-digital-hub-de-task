# MAN Digital Hub DE Task

This is the main repository for the MAN Digital Hub Data Engineering task. This repository will include the Lambda functions, AWS Athena DDL code of the tables, and the AWS Codeformation template to deploy the solution.